import path  from "path";
import webpack from "webpack";
import FriendlyErrorsWebpackPlugin from 'friendly-errors-webpack-plugin';
import notifier from 'node-notifier';

module.exports = [
  {
    devtool: "eval",
    entry: [
      "webpack-hot-middleware/client",
      "./client"
    ],
    output: {
      filename: "bundle.js",
      path: __dirname,
      publicPath: "/"
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          loaders: ["babel-loader"],
          exclude: /node_modules/
        },
        {
          test: /\.(css|scss)$/,
          loaders: ["style-loader", "css-loader", "sass-loader?sourceMap"],
          include: [
            path.join(__dirname, './src/assets/scss'),
          ]
        },
        {
          test: /\.(gif|png|jpe?g|svg)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                name:'src/assets/images/[hash].[ext]'
              }
            }
          ]
        },
        {
          test: /\.(eot|ttf|woff|woff2)$/,
          loader: "file-loader"
        }
      ]
    },
    resolve: {
      extensions: [".js"],
      alias: {
        request: 'browser-request'
      }
    },
    plugins: [
      new FriendlyErrorsWebpackPlugin({
        onErrors: (severity, errors) => {
          if (severity !== 'error') {
            return;
          }
          const error = errors[0];
          notifier.notify({
            title: "Webpack error",
            message: severity + ': ' + error.name,
            subtitle: error.file || ''
          });
        }
      }),
      new webpack.NamedModulesPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoEmitOnErrorsPlugin(),
      new webpack.DefinePlugin({
        "process.env.BROWSER": true,
        "process.env.NODE_ENV": JSON.stringify("development")
      })
    ]
  }
]
