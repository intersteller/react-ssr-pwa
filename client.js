import React from "react"
import { hydrate } from "react-dom"
import { ConnectedRouter } from "react-router-redux"
import { Provider } from "react-redux"
import * as OfflinePluginRuntime from 'offline-plugin/runtime'

import configureStore, { browserHistory } from "./src/store";
import { ENV } from "./src/config";
import "./src/assets/scss/styles.scss";
import App from "./src/App";

const preloadedState = window.__PRELOADED_STATE__;
delete window.__PRELOADED_STATE__;

const store = configureStore(preloadedState);
if (ENV === 'production' && typeof window !== 'undefined' && typeof window.navigator !== 'undefined') {
  OfflinePluginRuntime.install({
    onUpdateReady: () => OfflinePluginRuntime.applyUpdate(),
    onUpdated: () => window.swUpdate = true
  });
}

const renderContainer = Component => {
  const root = document.getElementById("root");

  if (root) {
    hydrate((
      <Provider store={store}>
        <ConnectedRouter history={browserHistory}>
          <Component />
        </ConnectedRouter>
      </Provider>
    ), root)
  }
};

renderContainer(App);

if (ENV !== "production" && module.hot) {
  module.hot.accept("./src/App", () => {
    const nextApp = require('./src/App').default
    renderContainer(nextApp);
  });

  module.hot.accept("./src/reducers", () => {
    const nextRootReducer = require("./src/reducers/index").default;
    store.replaceReducer(nextRootReducer);
  });
}
