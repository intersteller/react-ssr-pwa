import express from "express";
import http from "http";
import chokidar from 'chokidar';
import webpack from "webpack";
import webpackDevMiddleware from "webpack-dev-middleware";
import webpackHotMiddleware from "webpack-hot-middleware";

import { ENV, SITE_PORT, SITE_DOMAIN } from "./src/config"
const app = express();
app.set('port', SITE_PORT);
app.set('host', SITE_DOMAIN);

const config = ENV !== "production" ? require("./webpack.dev.js") : require("./webpack.prod.js");
const compiler = webpack(config);

app.use(
  webpackDevMiddleware(compiler, {
    quiet: true,
    noInfo: true,
    publicPath: "/",
    hot: true,
    serverSideRender: true,
    stats: {
      hash: false,
      version: false,
      timings: false,
      assets: false,
      chunks: false,
      modules: false,
      reasons: false,
      children: false,
      source: false,
      errors: false,
      errorDetails: false,
      warnings: false,
      publicPath: false
    },
    logLevel: "silent"
  })
);

if (ENV !== "production") {
  app.use(webpackHotMiddleware(compiler));

  const watcher = chokidar.watch('./server');

  watcher.on('ready', () => {
    watcher.on('change', () => {
      Object.keys(require.cache).forEach((id) => {
        if (/[\/\\]server[\/\\]/.test(id)) delete require.cache[id];
      });
    });
  });

  compiler.plugin('done', function() {
    Object.keys(require.cache).forEach(function(id) {
      if (/[\/\\]client[\/\\]/.test(id)) delete require.cache[id];
    });
  });
}

app.get('*', (req, res, next) => {
  const serverSide = require('./server');
  serverSide(req, res);
});

app.get('*.js', (req, res, next) => {
  req.url = req.url + '.gz';
  res.set('Content-Encoding', 'gzip');
  next();
});

const server = http.createServer(app);
server.listen(app.get('port'), app.get('host'));
