const path = require("path");
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const OfflinePlugin = require("offline-plugin");

module.exports = {
  devtool: "cheap-module-source-map",
  entry: [
    "./client"
  ],
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "public")
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: "babel-loader",
        exclude: /node_modules/,
        options: {
          presets: ["es2015", "react", "stage-0"],
        }
      },
      {
        test: /\.(css|scss)$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            {
              loader: "css-loader",
              options: {
                url: false,
                minimize: true,
                sourceMap: true
              }
            },
            {
              loader: "sass-loader",
              options: {
                sourceMap: true
              }
            }
          ]
        })
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name:'src/assets/images/[hash].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        loader: "file-loader"
      }
    ]
  },
  resolve: {
    extensions: [".js"]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: "css/styles.css",
      allChunks: true
    }),
    new CopyWebpackPlugin([
      {
        from: 'src/assets/images',
        to: 'images/',
        force: true
      },
      {
        from: 'src/layouts',
        to: '[name].[ext]',
        ignore: ["*.js"]
      }
    ], []),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new UglifyJSPlugin({
      sourceMap: true,
      cache: true
    }),
    new CompressionPlugin({
      asset: "[path].gz[query]",
      algorithm: "gzip",
      test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg?.+$/,
      threshold: 10240,
      minRatio: 0.8
    }),
    new OfflinePlugin({
      externals: ['/'],
      responseStrategy: "network-first",
      autoUpdate: 1000 * 60 * 15,
      ServiceWorker: {
        events: true,
        navigateFallbackURL: '/',
        prefetchRequest: {
          credentials: 'include'
        }
      },
      AppCache: {
        FALLBACK: {
          '/': '/index.html'
        }
      }
    }),
    new webpack.HashedModuleIdsPlugin(),
    new webpack.DefinePlugin({
      "process.env.BROWSER": true,
      "process.env.NODE_ENV": JSON.stringify("production")
    })
  ]
}
