import React from 'react'
import { renderToString } from 'react-dom/server'
import { StaticRouter } from 'react-router'
import { Helmet } from 'react-helmet'
import { Provider } from 'react-redux'
import { matchRoutes } from 'react-router-config'
import _ from "lodash"

import configureStore, { browserHistory } from "./src/store";

import Template from './src/layouts/template';
import App from "./src/App";
import routes from "./src/routes";

const serverRenderer = (req, res) => {
  const store = configureStore();

  let fetchArray = []
  const components = matchRoutes(routes, req.url);
  const promises = components.map(({ route }) => {
    if (req.originalUrl === route.path) {
      const { fetchData } = route.component
      if (fetchData !== undefined && !_.isEmpty(fetchData)) {
        fetchArray = fetchArray.concat(fetchData);
      }
    }

    const fetchData = fetchArray.map(cur => {
      if (cur instanceof Function) {
        return store.dispatch(cur())
      }

      return Promise.resolve(null)
    })

    return Promise.all(fetchData)
  });

  return Promise.all(promises).then(() => {
    const markup = renderToString(
      <Provider store={store}>
        <StaticRouter location={req.url} context={browserHistory}>
          <App/>
        </StaticRouter>
      </Provider>
    )
    const helmet = Helmet.renderStatic()

    res.status(200).send(Template({
      markup: markup,
      helmet: helmet,
      preloadedState: store.getState()
    }))
  })
}

module.exports = serverRenderer
