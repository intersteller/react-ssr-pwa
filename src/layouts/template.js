import { ENV } from "../config"

export default ({ markup, helmet, preloadedState }) => {
  const manifest = ENV === "production" ? '<link rel="manifest" href="/manifest.json">' : ''
  const styles = ENV === "production" ? '<link rel="stylesheet" href="/css/styles.css" type="text/css" />' : ''

  return `<!doctype html>
    <html ${helmet.htmlAttributes.toString()}>
    <head>
      <meta charset="utf-8">
      ${helmet.title.toString()}
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="black">
      <meta name="apple-mobile-web-app-title" content="Weather PWA">
      <meta name="msapplication-TileImage" content="images/icons/icon-144x144.png">
      <meta name="msapplication-TileColor" content="#2F3BA2">
      <meta name="theme-color" content="#ececec">
      ${helmet.meta.toString()}
      ${manifest}
      ${styles}
      <link rel="apple-touch-icon" href="images/icons/icon-152x152.png">
      ${helmet.link.toString()}
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body ${helmet.bodyAttributes.toString()}>
      <div id="root">${markup}</div>
      <script>window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}</script>
      <script src="/bundle.js" async></script>
      <noscript>Your browser does not support JavaScript!</noscript>
    </body>
    </html>`
}
