import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import weatherReducer from "./weatherReducer";

const reducer = combineReducers({
  router: routerReducer,
  weather: weatherReducer
})

export default reducer
