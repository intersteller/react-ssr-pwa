import * as types from "../actions/ActionTypes";

const initialState = {
  loading: true,
  location: false,
  data: {}
}

const weatherReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.REQUEST_START:
      return { ...state, loading: true }
    case types.REQUEST_SUCCESS:
      return { ...state, loading: false, data: action.weather, location: action.location }
    case types.REQUEST_ERROR:
      return { ...state, loading: false }
    default:
      return { ...state }
  }
}

export default weatherReducer
