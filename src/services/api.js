import axios from "axios";

export const fetchData = location => {
  const statement = `select * from weather.forecast where woeid=${location}`;
  const apiURL = `https://query.yahooapis.com/v1/public/yql?format=json&q=${statement}`;

  const localData = localStorage.getItem(location)
  if (localData !== null) {
    return JSON.parse(localData)
  }

  return axios.get(apiURL)
    .then(response => {
      const data = response.data.query.results.channel
      localStorage.setItem(location, JSON.stringify(data));

      return data
    })
    .catch(err => {
      console.log(err.message)
    })
}
