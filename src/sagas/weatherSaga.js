import 'regenerator-runtime/runtime';
import { takeLatest, put, call } from 'redux-saga/effects';
import * as types from "../actions/ActionTypes";
import { fetchData } from "../services/api";

function * fetchApi(action) {
  const location = !action.location ? 2459115 : action.location
  const weather = yield call(fetchData, location)

  if (weather) {
    yield put({ type: types.REQUEST_SUCCESS, weather, location: location })
  } else {
    yield put({ type: types.REQUEST_ERROR })
  }
}

export const weatherSaga = [
  takeLatest(types.REQUEST_START, fetchApi)
]
