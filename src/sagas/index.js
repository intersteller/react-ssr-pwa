import 'regenerator-runtime/runtime'
import { all } from 'redux-saga/effects'

import { weatherSaga } from "./weatherSaga";

export default function * root() {
  yield all([
    ...weatherSaga
  ])
}
