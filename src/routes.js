import Home from "./components/Homepage";
import NoMatch from "./components/NoMatch";

const routes = [
  {
    path: "/",
    exact: true,
    component: Home
  },
  {
    path: "*",
    component: NoMatch
  }
];

export default routes;
