import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware, { END } from 'redux-saga'
import { routerMiddleware } from 'react-router-redux';
import { composeWithDevTools } from 'redux-devtools-extension'
import { createBrowserHistory, createMemoryHistory } from 'history';

import rootReducer from './reducers';
import rootSagas from './sagas'
import { ENV } from './config'
export const browserHistory = process.env.BROWSER ? createBrowserHistory() : createMemoryHistory();
const historyMiddleware = routerMiddleware(browserHistory);

const configureStore = (initialState) => {
  const sagaMiddleware = createSagaMiddleware()
  const middleware = [sagaMiddleware, historyMiddleware]
  const composeEnhancers = ENV !== 'production' ?
    composeWithDevTools(applyMiddleware(...middleware)) :
    applyMiddleware(...middleware)

  const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers
  )

  let sagaTask = sagaMiddleware.run(function * () {
    yield rootSagas()
  })

  if (ENV !== 'production' && module.hot) {
    module.hot.accept('./reducers', () => {
      const nextRootReducer = require('./reducers').default
      store.replaceReducer(nextRootReducer)
    })

    module.hot.accept('./sagas', () => {
      const newSagas = require('./sagas').default
      sagaTask.cancel()
      sagaTask.done.then(() => {
        sagaTask = sagaMiddleware.run(function * replacedSaga(action) {
          yield newSagas()
        })
      })
    })
  }

  store.close = () => store.dispatch(END)

  return store
}

export default configureStore
