import * as types from "./ActionTypes"

export const weatherRequest = location => {
  return {
    type: types.REQUEST_START,
    location
  }
}
