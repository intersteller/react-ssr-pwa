import React, { Component, Fragment } from "react";
import { Switch } from "react-router-dom";
import { Helmet } from "react-helmet";
import { renderRoutes } from "react-router-config";

import routes from "./routes";

export default class App extends Component {
  render() {
    return (
      <Fragment>
        <Helmet
          htmlAttributes={{ lang: "en", amp: undefined }}
          titleTemplate="%s | Demo"
          titleAttributes={{ itemprop: "name", lang: "en" }}
          meta={[
            { name: "description", content: "Reacr SSR Service Worker" },
            { name: "viewport", content: "width=device-width, initial-scale=1.0" }
          ]}
        />

        <Fragment>
          <Switch>{renderRoutes(routes)}</Switch>
        </Fragment>
      </Fragment>
    );
  }
}
