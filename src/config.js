module.exports = {
  ENV: "development", // development | production
  SITE_PORT: 4000,
  SITE_DOMAIN: "0.0.0.0",
  WEBSITE_NAME: "React - SSR - PWA"
};
