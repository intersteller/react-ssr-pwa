import React, { Component, Fragment } from "react"
import { connect } from "react-redux"
import { Helmet } from "react-helmet"
import PropTypes from "prop-types"
import _ from "lodash"

import { weatherRequest } from "../actions/WeatherActions"
import Loading from "./Loading"
import CityModal from "./CityModal"
import WeatherContent from "./WeatherContent"

class Homepage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      openModal: false
    }
  }

  componentWillMount() {
    if (_.isEmpty(this.props.weather.data)) {
      this.props.weatherRequest()
    }
  }

  toggleModal() {
    this.setState({ openModal: !this.state.openModal })
  }

  refreshWeather() {
    this.props.weatherRequest(this.props.weather.location)
  }

  render() {
    const { loading, data } = this.props.weather
    let weatherContent = <Loading />
    if (!loading) {
      weatherContent = <WeatherContent weather={data} />
    }

    return (
      <Fragment>
        <Helmet title={data.title} />

        <header className="header">
          <h1 className="header__title">Weather PWA</h1>
          <button id="butRefresh" className="headerButton" aria-label="Refresh" onClick={() => this.refreshWeather()} />
          <button id="butAdd" className="headerButton" aria-label="Add" onClick={() => this.toggleModal()} />
        </header>

        {weatherContent}

        <CityModal
          isOpen={this.state.openModal}
          closeModal={() => this.toggleModal()}
        />
      </Fragment>
    )
  }
}

Homepage.propTypes = {
  weather: PropTypes.object.isRequired,
  weatherRequest: PropTypes.func.isRequired
}

Homepage.fetchData = [
  () => weatherRequest()
]

const mapStateToProps = (state) => {
  return {
    weather: state.weather
  }
}

export default connect(mapStateToProps, { weatherRequest })(Homepage)
