import React, { Component, Fragment } from "react"
import { Helmet } from "react-helmet"

class NoMatch extends Component {
  render() {
    return (
      <Fragment>
        <Helmet title="404 NotFound" />
        <h1>404 - Not Found</h1>
      </Fragment>
    )
  }
}

export default NoMatch
