import React, { Component } from "react"
import { connect } from "react-redux"
import PropTypes from "prop-types"

import { weatherRequest } from "../../actions/WeatherActions"

class CityModal extends Component {
  constructor(props) {
    super(props)

    this.state = {
      location: this.props.weather.location
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.location !== nextProps.weather.location) {
      this.setState({ location: nextProps.weather.location })
    }
  }

  handleChange(e) {
    this.setState({ location: e.target.value })
  }

  addNewLocation() {
    this.props.weatherRequest(this.state.location)
    this.props.closeModal()
  }

  render() {
    const modalContainerClass = "dialog-container" + (this.props.isOpen ? " dialog-container--visible" : "")

    return (
      <div className={modalContainerClass}>
        <div className="dialog">
          <div className="dialog-title">Add new city</div>
          <div className="dialog-body">
            <select id="selectCityToAdd" onChange={e => this.handleChange(e)} value={this.state.location}>
              <option value="2357536">Austin, TX</option>
              <option value="2367105">Boston, MA</option>
              <option value="2379574">Chicago, IL</option>
              <option value="2459115">New York, NY</option>
              <option value="2475687">Portland, OR</option>
              <option value="2487956">San Francisco, CA</option>
              <option value="2490383">Seattle, WA</option>
            </select>
          </div>
          <div className="dialog-buttons">
            <button className="button" onClick={() => this.addNewLocation()}>Add</button>
            <button className="button" onClick={() => this.props.closeModal()}>Cancel</button>
          </div>
        </div>
      </div>
    )
  }
}

CityModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  weatherRequest: PropTypes.func.isRequired
}

const mapStateToProps = state => {
  return {
    weather: state.weather
  }
}

export default connect(mapStateToProps, { weatherRequest })(CityModal)
