import React, { Component } from "react"
import PropTypes from "prop-types"

import { weatherIconClass } from "../../../helper"

class WeatherItem extends Component {
  render() {
    const { item } = this.props
    const weatherClass = `icon ${weatherIconClass(item.code)}`

    return (
      <div className="oneday">
        <div className="date">{item.day}</div>
        <div className={weatherClass} />
        <div className="temp-high">
          <span className="value">{item.high}°</span>
        </div>
        <div className="temp-low">
          <span className="value">{item.low}°</span>
        </div>
      </div>
    )
  }
}

WeatherItem.propTypes = {
  item: PropTypes.object.isRequired
}

export default WeatherItem
