import React, { Component } from "react"

import { weatherIconClass } from "../../helper"
import WeatherItem from "./WeatherItem"

class WeatherCity extends Component {
  render() {
    const { weather } = this.props
    const weatherClass = `icon ${weatherIconClass(weather.item.condition.code)}`
    const daysOfWeek = weather.item.forecast.map((item, index) => {
      if (index > 0 && index < 8) {
        return <WeatherItem key={index} item={item} />
      }
    })

    return (
      <main className="main">
        <div className="card cardTemplate weather-forecast">
          <div className="city-key" hidden />
          <div className="card-last-updated" hidden />
          <div className="location">{weather.location.city}, {weather.location.region}</div>
          <div className="date">{weather.lastBuildDate}</div>
          <div className="description">{weather.item.condition.text}</div>
          <div className="current">
            <div className="visual">
              <div className={weatherClass} />
              <div className="temperature">
                <span className="value">{weather.item.condition.temp}</span>
                <span className="scale">°F</span>
              </div>
            </div>
            <div className="description">
              <div className="humidity">{weather.atmosphere.humidity}%</div>
              <div className="wind">
                <span className="value" />
                <span className="scale">{weather.wind.speed} mph</span>
                <span className="direction"> {weather.wind.direction}°</span>
              </div>
              <div className="sunrise">{weather.astronomy.sunrise}</div>
              <div className="sunset">{weather.astronomy.sunset}</div>
            </div>
          </div>
          <div className="future">
            {daysOfWeek}
          </div>
        </div>
      </main>
    )
  }
}

export default WeatherCity
